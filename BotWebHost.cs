﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Telegram.Bot;
using System.Text.Json;
using Telegram.Bot.Types.InlineQueryResults;
using System.Text.Json.Serialization;

namespace telebot
{
    public class BotWebHost
    {
        class cartItem
        {
            public string description { get; set; }
            public string name { get; set; }
            public float price { get; set; }
            public Guid id { get; set; }
            public int quantity { get; set; }
        }
        class webresp
        {
            public string queryId { get; set;}
            public List<cartItem> items { get; set; }
            public float total { get; set; }
        }
        private HttpListener _listener;
        private int _port = 5010;
        TelegramBotClient _botClient;


    public BotWebHost(TelegramBotClient botClient)
        {
            _botClient = botClient;
            try
            {
                _listener = new HttpListener();
                _listener.Prefixes.Add("http://*:" + _port.ToString() + "/");
                _listener.Start();
                Console.WriteLine($"start listening on ");
                Receive();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Bot init exception: " + ex.Message);
            }
        }

        void Receive()
        {
            _listener.BeginGetContext(new AsyncCallback(listenerHandle), _listener);
        }

        async void listenerHandle(IAsyncResult result)
        {
            if (_listener.IsListening)
            {
                
                var context = _listener.EndGetContext(result);
                var request = context.Request;
                if (request.HasEntityBody)
                {
                    try
                    {
                        var body = request.InputStream;
                        var encoding = request.ContentEncoding;
                        var reader = new StreamReader(body, encoding);
                        var data = reader.ReadToEnd();
                        reader.Close();
                        body.Close();
                        Console.WriteLine(data);
                        var cart = System.Text.Json.JsonSerializer.Deserialize<webresp>(data);
                        var resplist = cart.items.Select(_ => _.name).ToList();
                        string respText = "";
                        foreach (var item in resplist) { respText += item + "\n"; }
                        Console.WriteLine("queryid " + cart.queryId);
                        Console.WriteLine("total" + cart.total.ToString() + ", " +  respText);
                        if (respText == "") respText = "нет позиций";
                        await _botClient.AnswerWebAppQueryAsync(
                            cart.queryId.ToString(),
                            new InlineQueryResultArticle(cart.queryId, $"Вы оформили заказ на {cart.total.ToString()} руб. \n",
                            new InputTextMessageContent(respText))
                            );
                    }
                    catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                }
                var responce = context.Response;
                responce.StatusCode = (int)HttpStatusCode.OK;
                responce.ContentType = "application/json";
                responce.OutputStream.Write(new byte[] { }, 0, 0);
                responce.OutputStream.Close();
                Receive();
            }
        }
    }
}
