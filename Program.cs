﻿// See https://aka.ms/new-console-template for more information

using Microsoft.Extensions.Configuration;
using System.Net;
using System.Reflection.Metadata.Ecma335;
using telebot;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;



var builder = new ConfigurationBuilder();
builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
var devEnvironmentVariable = Environment.GetEnvironmentVariable("NETCORE_ENVIRONMENT");
var isDevelopment = string.IsNullOrEmpty(devEnvironmentVariable) || devEnvironmentVariable.ToLower() == "development";
if (isDevelopment)
{
    builder.AddUserSecrets<Program>();
}


var Configuration = builder.Build();
var prxUser = Configuration["prxUser"];
var prxPsw = Configuration["prxPsw"];
var prxDmn = Configuration["prxDmn"];
var botKey = Configuration["botKey"];
var prx = Configuration["prx"];


/*
var proxy = new WebProxy(Host: prx, Port: 8080)
{
    Credentials = new NetworkCredential(prxUser, prxPsw, prxDmn),
};
var clientHandler = new HttpClientHandler();
//clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyError) => { return true; };
clientHandler.Proxy = proxy;

var httpCllient = new HttpClient(clientHandler);// { Proxy = proxy, }) { BaseAddress = new Uri("https://api.telegram.org/") };
*/
var tbcoptions = new TelegramBotClientOptions("5689949766:AAHfq1v8QF2YjJ0xRQESKEe7KfA7Np4rbu8");
var botClient = new TelegramBotClient(tbcoptions/*, httpCllient*/);

BotWebHost host = new BotWebHost(botClient);

List<UserInfo> users = new List<UserInfo>();

using CancellationTokenSource cts = new();

// StartReceiving does not block the caller thread. Receiving is done on the ThreadPool.
ReceiverOptions receiverOptions = new()
{
    AllowedUpdates = Array.Empty<UpdateType>() // receive all update types 
};

botClient.StartReceiving(
    updateHandler: HandleUpdateAsync,
    pollingErrorHandler: HandlePollingErrorAsync,
    receiverOptions: receiverOptions,
    cancellationToken: cts.Token
);

var me = await botClient.GetMeAsync();

Console.WriteLine($"Start listening for @{me.Username}");
var waiter = new ManualResetEventSlim(false);
waiter.Wait();
Console.WriteLine("stopping");

// Send cancellation request to stop bot
cts.Cancel();

async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
{
    UserInfo? user = null;
    // Only process Message updates: https://core.telegram.org/bots/api#message
    foreach(UserInfo userInfo in users)
    {
        if(userInfo.chatId == update.Message?.Chat.Id)
        {
            user = userInfo;
            break;
        }
    }
    if (update.Message is not { } message)
        return;
    var chatId = message.Chat.Id;
    if (update.Message?.WebAppData?.Data is not null)
    {
       var msg = await botClient.SendTextMessageAsync(
       chatId: chatId,
       text: $"успешный логин - {update.Message?.WebAppData?.Data}",
       cancellationToken: cancellationToken);
        return;
    }

    Console.WriteLine(update.Message?.Contact?.PhoneNumber);
    
    // Only process text messages
    
    
    if (user is null) { user = new UserInfo() {chatId =  chatId, Phone = update.Message.Contact?.PhoneNumber};}

    var msgT = (message.Text is not { } messageText);

    if (!msgT) Console.WriteLine($"Received a '{message.Text}' message in chat {chatId}.");
    else Console.WriteLine($"Received a '{message}' message in chat {chatId}.");

    if(!msgT && (message.Text == "/start"))
    {
        var mu = new ReplyKeyboardMarkup(
             new KeyboardButton("Войти") 
             { WebApp = new WebAppInfo { Url = "https://bustlergreen.ru/login" } })
        {ResizeKeyboard= true, OneTimeKeyboard = true };

        var msg = await botClient.SendTextMessageAsync(
        chatId: chatId,
        text: "войдите в свою учетную запись",
        replyMarkup: mu, 
        cancellationToken: cancellationToken);
        return;
    }

    var kb = new InlineKeyboardButton("сделать заказ");
      kb.WebApp = new WebAppInfo() { Url = "https://bustlergreen.ru" };
    var rkm = new InlineKeyboardMarkup(kb);
        // Echo received message text
    
    Message sentMessage = await botClient.SendTextMessageAsync(
        chatId: chatId,
        text: !msgT ? $"replay {message.Text}" : "обработано",
        replyMarkup: rkm,
        cancellationToken: cancellationToken);
}

Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
{
    var ErrorMessage = exception switch
    {
        ApiRequestException apiRequestException
            => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
        _ => exception.ToString()
    };
    Console.WriteLine(ErrorMessage);
    return Task.CompletedTask;
}

Console.ReadLine();

class UserInfo
{
    public long chatId;
    public string? Phone;
}