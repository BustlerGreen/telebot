#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/runtime:6.0
EXPOSE 5010/tcp
COPY bin/Debug/net6.0 .
ENTRYPOINT ["dotnet", "telebot.dll"]